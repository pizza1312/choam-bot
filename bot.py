#!/usr/bin/env python3

import os, time
import requests
import discord
import re

from dotenv import load_dotenv

load_dotenv()

TOKEN = os.getenv("DISCORD_TOKEN")
GUILD = os.getenv("GUILD")

intents = discord.Intents().all()
client = discord.Client(intents=intents)

gm_channels = ["gm-discussion","bot-testing", "private"]
bot_channel = "choam-services"
gm_roles = ["GM","Admin"]
debug_channel = None

sheeturl = "https://sheetdb.io/api/v1/c5j5b7ydw336y"

STATS = ["Industry", "Technology", "Economy", "Military", "Espionage", "Underground", "Propaganda", "Honour", "Devotion", "Mentat", "Truthsayer", "Wpn Master"]
SKILLS = ["Industry", "Technology", "Economy", "Military", "Espionage", "Underground", "Propaganda", "Honour", "Devotion"]

def signed_str(string):
    if int(string)<0:
        return str(string)
    else:
        return "+"+str(string)

def fillout(str, length):
    if len(str)>length:
        return str[:length]
    else:
        return str + ((length-len(str))*" ")
 
# Function to validate 
# hexadecimal color code . 
def is_hex_colour(str):
 
    # Regex to check valid 
    # hexadecimal color code.
    regex = "^([A-Fa-f0-9]{6})$"
 
    # Compile the ReGex 
    p = re.compile(regex)
 
    # If the string is empty 
    # return false
    if(str == None):
        return False
 
    # Return if the string 
    # matched the ReGex
    if(re.search(p, str)):
        return True
    else:
        return False

def is_int(v):
    try:
        i=int(v)
    except ValueError:
        return False
    return True

def is_admin(member):
    for r in member.roles:
        if r.name in gm_roles:
            return True
    return False

def get_house_slug(name):
    slug = ""
    if " " in name:
        words = name.split(" ")
        for w in words:
            slug += w[0]
            if len(slug) >= 3:
                break
        if len(slug) < 3:
            slug += words[-1][1]
    else:
        slug = name[0:3]
    return slug

def get_user_house(user):
    r = get_user_house_role(user)
    if r:
        return r.name.split(" ",1)[1]
    return None

def get_house_role(guild, string):
    print("checking",string)
    for r in guild.roles:
        if "House " in r.name:
            if string.lower() in r.name.lower():
                return r
    return None

def get_user_house_role(user):
    for r in user.roles:
        if "House" in r.name:
            return r
    return None

def get_player_role(guild):
    for r in guild.roles:
        if r.name == "Player":
            return r
    return None

async def m_append(message, text):
    old_contents=message.content
    return await message.edit(content=old_contents+"\n"+text)
    

def get_new_cat_position(guild, cat_name):
    gcs = guild.by_category()
    

    print("got",len(gcs),"categories:")
    print(" ".join([c[0].name for c in gcs]))

    # iterate along, comparing names
    # first check if house categories exist
    if "- House" not in " ".join([c[0].name for c in gcs]):
        return len(gcs)-1
    # okay so they do exist
    # iterate over all of them, wait until houses identified
    got_houses=False
    for i in range(len(gcs)):
        c = gcs[i][0]

        if c.name.startswith("- House"):
            
            got_houses=True
            # gottem, time to compare
            if c.name > cat_name:
                return i-1
        elif got_houses:
            #reached the end of the houses, finish up
            return i

    return len(gcs)
    
def make_sheet_embed(data, colour=None):

    # make the stats bit
    stats = "```"

    for stat in SKILLS:
        spec = data[stat+"_spec"]
        if spec!="":
            spec = "["+spec+"]"
        line = fillout(stat,13)+fillout(data[stat],3)+spec
        stats = stats+"\n"+line
    stats = stats+"```"
    

    if colour:
        e = discord.Embed(title="House "+data["House Name"], colour=colour, description=stats)
    else:
        e = discord.Embed(title="House "+data["House Name"], description=stats)
              
    e.set_thumbnail(url=crest_url_from_id(data["Crest_id"]))

    print("got crest",crest_url_from_id(data["Crest_id"]))
    return e

def crest_url_from_id(idval):
    return "https://adm.pizza/landsraad/"+idval+".png"

def house_crest_url(house):
    r=requests.get(sheeturl+"/search?House Name="+house+"&sheet=Bot")

    if len(r.json())==0:
        return None

    data=r.json()[0]

    return crest_url_from_id(data["Crest_id"])


async def process_bot_command(message):
    command = message.content[1:]
    commands = message.content[1:].split(" ")
    
    if commands[0] == "fear":
        response = "I must not fear. Fear is the mind-killer. Fear is the little-death that brings total obliteration. I will face my fear. I will permit it to pass over me and through me. And when it has gone past I will turn the inner eye to see its path. Where the fear has gone there will be nothing. Only I will remain."
        await message.channel.send(response)
    
    elif commands[0] == "version":
        date = os.path.getmtime(os.path.realpath(__file__))
        await message.channel.send("**CHOAM Listings bot** by Pizza \n*Last updated: "+time.ctime(date)+"*")

    ### CHARACTER SHEET DISPLAY ###
    elif commands[0] == "sheet":
        cont=True
        house=""
        # Check channel
        if message.channel.name in gm_channels:
            # ok for any request, interpret argument
            if " " in command:
                house=command.split(" ",1)[1]
            else:
                await message.channel.send("*Please specify the House for enquiry. Example:* `!sheet Atreides`")
                cont=False
        elif "private" in message.channel.name:
            # only ok for request of appropriate person, no point interpreting argument, just get house name
            house = get_user_house(message.author)
            if not house:
                cont=False
                roles = "`"
                for r in message.channel.changed_roles:
                    roles = roles + r.name + "` `"
                roles = roles[:-1]
                await message.channel.send("*Error: insecure channel? Roles:* "+roles)
        else:
            cont=False
            await message.channel.send("*We request that this communication take place across a more secure channel. Thank you for your understanding.*")

        if cont:
            r=requests.get(sheeturl+"/search?House Name="+house+"&sheet=Bot")
            print(r.json())

            if len(r.json())==0:
                await message.channel.send("*No entries found under House "+house+".*")

            else:
                data=r.json()[0]
                e = make_sheet_embed(data, colour=get_house_role(message.guild, data["House Name"]).color)
                await message.channel.send(embed=e)

                """
                url = "https://sheetdb.io/api/v1/sllgumbz286o7?sheet=trades"
                data["id"]="INCREMENT"
                data["approved"] = "FALSE"
                data["active"] = "FALSE"
                data["expiry"] = ""
                d = data
                x=requests.post(url, data=d)
                """
                
   


    elif commands[0] == "list_houses":
        msg = "## Houses:"
        for r in message.guild.roles:
            if r.name.startswith("House "):
                msg += "\n- <@&"+str(r.id)+">"

        await message.channel.send(msg)

    elif command == "stilgar":
        await message.channel.send("https://youtu.be/l_kvakUCIYc")

    elif command == "help":
        reply = "### At your service, my lord. Here are some of the services the Combine Honnete Ober Advancer Mercantiles can provide:\n"
        reply += "- `!sheet` - *View the abilities of your House (over a secure channel, of course).*\n"
        #reply += "`!trades` - *Request all ongoing trade listings available for viewing.*\n"
        reply += "- `!set_house_colour` - *To inform us of a change in your preferred heraldry.*\n"
        reply += "- `!fear` - *Lest you require reassurance.*\n"
        await message.channel.send(reply)

    elif command == "gm_commands":
        if message.channel.name not in gm_channels:
            await message.channel.send("*We request that this communication take place across a more secure channel. Thank you for your understanding.*")
            return
        else:
            date = os.path.getmtime(os.path.realpath(__file__))
            msg = "### GM Commands\n"        
            msg += "*Bot last updated: "+time.ctime(date)+"*\n"
            msg += "- `!setup_house Atreides` - sets up a House on the server, incl role, category and channels.\n"
            msg += "- `!add [player_id] @House Atreides` - Adds a player to a House. Right-click on a player to get their ID.\n"
            msg += "*If adding roles manually, don't forget to include the @Player role as well as the House role.*"        
            await message.channel.send(msg)
    
    elif command == "crest":
        await message.channel.send(house_crest_url(get_user_house(message.author)))        

    elif commands[0] == "crest":

        #okay, get the house then send the crest
        house = get_house_role(message.guild, " ".join(commands[1:]))
        if not house:
            await message.channel.send("*Apologies, we have no formal records of anybody under that name.*")
            return

        await message.channel.send(house_crest_url(house.name.replace("House ","")))
        
        

    elif commands[0] == "slugify":
        await message.channel.send(get_house_slug(" ".join(commands[1:])))

    ## SETUP NEW HOUSE ##
    #TODO: Ensure GM access only
    
    elif commands[0] == "setup_house":
        if message.channel.name not in gm_channels:
            await message.channel.send("*We request that this communication take place across a more secure channel. Thank you for your understanding.*")
            return
        else:
            housename = " ".join(commands[1:]).title()
            await message.channel.send("## Initialising House "+housename)

            updates = await message.channel.send("- Creating role & perms...")

            houserole = await message.guild.create_role(name="House "+housename)
            prole = get_player_role(message.guild)

            # Perms for Correspondence: players can interact
            corr_perms = {
                message.guild.default_role: discord.PermissionOverwrite(send_messages=False),
                houserole: discord.PermissionOverwrite(send_messages=True),
                prole: discord.PermissionOverwrite(send_messages=True)
            }
            # Perms for GM Chat: nobody can see except house
            gmchat_perms = {
                message.guild.default_role: discord.PermissionOverwrite(read_messages=False),
                houserole: discord.PermissionOverwrite(read_messages=True)
            }
            # Perms for category: all can see, House can interact
            category_perms = {
                message.guild.default_role: discord.PermissionOverwrite(send_messages=False),
                houserole: discord.PermissionOverwrite(send_messages=True)
            }
            
            updates = await m_append(updates,"- Creating category...")

            cat_name = "- House "+housename+" -"
            pos = get_new_cat_position(message.guild, cat_name)
            print("position:",pos)
            category = await message.guild.create_category(cat_name, position=pos, overwrites=category_perms)

            updates = await m_append(updates,"- Creating channels...")

            
            await message.guild.create_text_channel("house-details", category=category)
            await message.guild.create_text_channel("correspondence", category=category, overwrites=corr_perms)
            await message.guild.create_text_channel(get_house_slug(housename)+"-private", category=category, overwrites=gmchat_perms)
            
            
            updates = await m_append(updates,"**House setup complete**. Add House members with `!add [player_id] @House "+housename+"`.")

    elif commands[0] == "add":
        if not is_admin(message.author):
            await message.channel.send("*You don't appear to have the clearance to make this request. Could you perhaps contact a superior?*")
            return
        else:
            """
            if message.channel.name not in gm_channels:
                await message.channel.send("*We request that this communication take place across a more secure channel. Thank you for your understanding.*")
                return
            """

            commands[1] = commands[1].replace("<","").replace(">","").replace("&","").replace("@","")
            houseid = commands[2].replace("<","").replace(">","").replace("&","").replace("@","")
            houserole = message.guild.get_role(int(houseid))

            
            prole = get_player_role(message.guild)
            print("houserole:",houserole)
            print("prole:",prole)

            if is_int(commands[1]):
                print("int",commands[1])
                mbr = message.guild.get_member(int(commands[1]))
                print("got",mbr)
                pid = commands[1]
            else:
                print("name",commands[1])
                mbr = message.guild.get_member_named(commands[1])
                pid = mbr.id

            await mbr.add_roles(houserole, prole)
            
            await message.channel.send("Added player <@"+str(pid)+"> to <@&"+str(houseid)+">.")
            

    elif commands[0] == "set_house_colour":
        house = get_user_house_role(message.author)
        synerr = "*Apologies my lord, would you care to clarify? The correct procedure for your request is attached below:*"
        synerr += "\n`!set_house_colour cdcd00` (hex colour code)"
        if not house:
            await message.channel.send("*Your request has been rejected. We advise you contact a figure of authority within your House to issue the request for you.*")
            return
        if len(commands) != 2:
            await message.channel.send(synerr)
            return

        # first check if colour format is okay
        if not is_hex_colour(commands[1]):
            await message.channel.send(synerr)
            return

        # got it, make the string
        try:
            await house.edit(color = discord.Color.from_str("#"+commands[1]))
        except ValueError:
            await message.channel.send(synerr)
            return

        await message.channel.send("*Your heraldry has been adjusted accordingly.*")
        return

    elif command == "":
        pass

    elif command == "am_i_admin":
        if is_admin(message.author):
            await message.channel.send("You are admin")
        else:
            await message.channel.send("You are not admin")
        
    else:
        await message.channel.send("*Request* `"+command+"` *not understood. Our sincerest apologies.*")


@client.event
async def on_ready():
    guild = discord.utils.get(client.guilds, name = GUILD)
    print('Connected to',guild.name,'as',client.user)
    #for m in guild.members:
    #    print("- "+m.name)

    # Print successful startup message
    debug_channel = discord.utils.get(guild.channels, name="choam-laboratory")
    #await debug_channel.send("**CHOAM listings available for consultation.**")


@client.event
async def on_message(message):
    if "!" in message.content and message.content[0] == "!":
        print(message.content)
        await process_bot_command(message)

client.run(TOKEN)
