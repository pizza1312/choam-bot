#!/usr/bin/env bash

echo "Restarting CHOAM bot..."
systemctl stop choam-bot.service
sleep 5
systemctl start choam-bot.service
echo "Bot restarted."